module bitbucket.org/n-gl/ngl

go 1.18

require (
	bitbucket.org/tentontrain/math v0.0.0-20220519191623-a4e86beba92a
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-gl/gl v0.0.0-20211210172815-726fda9656d6
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/veandco/go-sdl2 v0.4.10
	golang.org/x/exp v0.0.0-20220518171630-0b5c67f07fdf // indirect
	golang.org/x/image v0.0.0-20220413100746-70e8d0d3baa9
)
