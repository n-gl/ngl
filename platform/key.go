package platform

import "time"

type KeyType int

//go:generate stringer -type=KeyType
const (
	// These only control
	KeyNone KeyType = iota
	KeyControlDown
	KeyControlUp
	KeyAltDown
	KeyAltUp
	KeyBackspace
	KeyDelete
	KeyLeft
	KeyRight
	KeyUp
	KeyDown
	// These have text
	KeySpace
	KeyNewline
	KeyText
)

func (k KeyType) HasText() bool {
	if k <= KeyBackspace {
		return false
	}
	return true
}

type KeyPress struct {
	Key       KeyType
	Text      string
	Timestamp time.Time
}
