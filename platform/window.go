package platform

import (
	"runtime"

	"github.com/veandco/go-sdl2/sdl"
)

// InitializeWindow a window of width x height and set its name
func InitializeWindow(width, height int, name string, resizeable bool) (*sdl.Window, error) {
	// sdl might crash otherwise
	runtime.LockOSThread()

	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		return nil, err
	}

	var options uint32 = sdl.WINDOW_OPENGL

	if resizeable {
		options = options | sdl.WINDOW_RESIZABLE
	}

	window, err := sdl.CreateWindow(name, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		int32(width), int32(height), options)
	if err != nil {
		return nil, err
	}

	if _, err = window.GLCreateContext(); err != nil {
		return nil, err
	}
	return window, nil
}
