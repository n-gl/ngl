package main

import (
	"fmt"
	"time"

	"bitbucket.org/n-gl/ngl/color"
	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/n-gl/ngl/sprite/shaders"
	"bitbucket.org/tentontrain/math"
	"github.com/veandco/go-sdl2/sdl"
)

func main() {
	window, err := sprite.InitializeWindow(600, 600, "simple", true)
	if err != nil {
		panic(err)
	}

	renderer := sprite.Renderer{}
	renderer.Init()
	shader, err := shaders.NewColorSpriteShader()
	if err != nil {
		panic(err)
	}

	fmt.Println(shader.Attributes)

	store, err := sprite.NewSpriteStoreFromFiles("../data/pumpkin_dude.png",
		"../data/pumpkin_dude.atlas", shader, 12, math.Point{600, 600})
	if err != nil {
		panic(err)
	}

	renderer.AddSpriteStore(store, 1)

	start := time.Now()
	for {
		event := sdl.PollEvent()
		switch t := event.(type) {

		case *sdl.WindowEvent:
			if t.Event == sdl.WINDOWEVENT_RESIZED {
				renderer.Resize(int(t.Data1), int(t.Data2))
				fmt.Println(int(t.Data1), int(t.Data2))
			}
		}

		store.Empty()

		sprits, err := sprite.NewSprite(0, store)
		if err != nil {
			panic(err)
		}
		sprits.SetSize(math.Point{150, 150})
		sprits.SetPosition(math.Point{300, 0})
		sprits.SetColor(color.COLOR_MAGENDA)

		fmt.Println(sprits.GetColor())

		if err := store.AddSprite(sprits); err != nil {
			panic(err)
		}

		if time.Since(start) > time.Second*3 {
			renderer.SetBGColor([4]float32{1, 0, 0, 1})
		}

		if time.Since(start) > time.Second*10 {
			return
		}

		renderer.Render()
		window.GLSwap()
	}
}
