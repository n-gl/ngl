package main

import (
	"time"

	"bitbucket.org/n-gl/ngl/platform"
	"bitbucket.org/n-gl/ngl/shaders"
	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/n-gl/ngl/text"
	"bitbucket.org/n-gl/ngl/ui"
	"bitbucket.org/tentontrain/math"
	"github.com/davecgh/go-spew/spew"
	"github.com/veandco/go-sdl2/sdl"
)

func main() {
	window, err := platform.InitializeWindow(600, 600, "simple", true)
	if err != nil {
		panic(err)
	}

	renderer := sprite.Renderer{}
	renderer.Init()

	shader, err := shaders.NewColorSpriteShader()
	if err != nil {
		panic(err)
	}

	store, err := sprite.NewSpriteStoreFromFiles("../data/utah.png", "../data/utah.atlas",
		shader, 12, math.Vector2[int]{600, 600})
	if err != nil {
		panic(err)
	}

	renderer.AddSpriteStore(store, 3)

	mainWindow := ui.MainWindow{}
	mainWindow.UI.BoundingBox.P2 = math.Vector2[int]{600, 600}

	// fills whole window
	imageBG := ui.NewImage(0, store)
	mainWindow.AddChild(imageBG)

	// offset py a fixed 10 pixels at the bottom left
	box := ui.NewFixedSizeBox(math.Vector2[int]{10, 10}, math.Vector2[int]{200, 200})
	mainWindow.AddChild(box)
	imageBottonLeft := ui.NewImage(0, store)
	box.AddChild(imageBottonLeft)

	// top left
	aBox2 := ui.NewAnchoredFixedSizeBox(math.Vector2[int]{0, 100}, math.Vector2[int]{0, -100}, math.Vector2[int]{100, 100})
	imageAbox2 := ui.NewImage(0, store)
	aBox2.AddChild(imageAbox2)
	mainWindow.AddChild(aBox2)

	// splits
	vSplit := ui.VerticalSplitBox{Start: 50, End: 100}
	mainWindow.AddChild(&vSplit)
	imageVsplit := ui.NewImage(0, store)
	vSplit.AddChild(imageVsplit)

	hSplit := ui.HorizontalSplitBox{Start: 33, End: 66}
	vSplit.AddChild(&hSplit)

	textMap, err := text.NewTextMap("../data/font.ttf", 32, text.CharacterSetASCII())
	if err != nil {
		panic(err)
	}

	textStore, err := sprite.NewSpriteStore(textMap.FontAtlas.AtlasImage, textMap.ToAtlas(), shader, 128, math.Vector2[int]{600, 600})
	renderer.AddSpriteStore(textStore, 2)
	if err != nil {
		panic(err)
	}

	hSplit2 := ui.HorizontalSplitBox{Start: 66, End: 100}
	vSplit.AddChild(&hSplit2)
	textString := ui.NewTextString("Some Text", textMap, textStore)
	hSplit2.AddChild(textString)

	//window border
	borderStore, err := sprite.NewSpriteStoreFromFiles("../data/border.png", "../data/border.atlas",
		shader, 32, math.Vector2[int]{600, 600})
	if err != nil {
		panic(err)
	}
	renderer.AddSpriteStore(borderStore, 1)
	border := ui.NewBorder(1, 2, 0, 3, 4, 5, 6, 7, borderStore)
	hSplit.AddChild(border)
	imageHsplit := ui.NewImage(0, store)
	border.AddChild(imageHsplit)

	mainborder := ui.NewBorder(1, 2, 0, 3, 4, 5, 6, 7, borderStore)
	mainWindow.AddChild(mainborder)

	// vstack
	vstack := ui.VerticalStack{Reverse: true}
	vstack.AddChild(ui.NewTextString("Stack 1", textMap, textStore))
	vstack.AddChild(ui.NewTextString("Stack 2", textMap, textStore))
	vstack.AddChild(ui.NewTextString("Stack 3", textMap, textStore))
	vstack.AddChild(ui.NewTextString("Stack 4", textMap, textStore))
	vstack.AddChild(ui.NewTextString("Stack 5", textMap, textStore))

	hSplit3 := ui.HorizontalSplitBox{Start: 0, End: 32}
	hSplit3.AddChild(&vstack)
	vSplit.AddChild(&hSplit3)

	for {

		event := sdl.PollEvent()
		switch t := event.(type) {
		case *sdl.WindowEvent:
			if t.Event == sdl.WINDOWEVENT_RESIZED {
				renderer.Resize(int(t.Data1), int(t.Data2))
				mainWindow.UI.BoundingBox.P2 = math.Vector2[int]{int(t.Data1), int(t.Data2)}
			}
		}

		store.Empty()
		textStore.Empty()
		borderStore.Empty()

		mainWindow.Update(0)
		sprites := ui.DrawTree(&mainWindow)

		for i := range sprites {
			if err := renderer.AddSprite(sprites[i]); err != nil {
				spew.Dump(sprites[i])
				panic(err)
			}
		}

		time.Sleep(time.Millisecond * 50)
		renderer.Render()
		window.GLSwap()
	}
}
