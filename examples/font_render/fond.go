package main

import (
	"time"

	"bitbucket.org/n-gl/ngl/color"
	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/n-gl/ngl/sprite/shaders"
	"bitbucket.org/n-gl/ngl/text"
	"bitbucket.org/tentontrain/math"
)

var sampleText = "I've studied now Philosophy and Jurisprudence, Medicine, and even, alas! Theology."

func main() {
	window, err := sprite.InitializeWindow(600, 600, "fondling", false)
	if err != nil {
		panic(err)
	}

	renderer := sprite.Renderer{}
	renderer.Init()

	ascii := text.CharacterSetASCII()
	greek := text.CharacterSetGreek()
	both := append(ascii, greek...)

	shader, err := shaders.NewColorSpriteShader()
	if err != nil {
		panic(err)
	}

	textMap, err := text.NewTextMap("../data/font.ttf", 32, both)
	if err != nil {
		panic(err)
	}

	store, err := sprite.NewSpriteStore(textMap.FontAtlas.AtlasImage, textMap.ToAtlas(), shader, 128, math.Point{600, 600})
	renderer.AddSpriteStore(store, 1)
	if err != nil {
		panic(err)
	}
	// manual character placement
	sprite, err := text.NewTextSprite('N', textMap, store)
	sprite.SetPosition(math.Point{10, 10})
	sprite.SetColor(color.COLOR_RED)
	store.AddSprite(sprite)

	sprite, err = text.NewTextSprite('g', textMap, store)
	sprite.SetPosition(math.Point{30, 30})
	sprite.SetColor(color.COLOR_GREEN)
	store.AddSprite(sprite)

	sprite, err = text.NewTextSprite('λ', textMap, store)
	sprite.SetPosition(math.Point{50, 50})
	sprite.SetColor(color.COLOR_BLUE)
	store.AddSprite(sprite)

	sprites := text.TextToSprites([][]rune{[]rune(sampleText)}, textMap, store, math.Box{}.New(300, 300, 600, 600), 1, 1, 1)
	for i := range sprites {
		store.AddSprite(sprites[i])
	}

	for {
		time.Sleep(time.Millisecond * 100)
		renderer.Render()
		window.GLSwap()
	}
}
