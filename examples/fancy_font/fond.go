package main

import (
	"time"

	"bitbucket.org/n-gl/ngl/color"
	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/n-gl/ngl/sprite/shaders"
	"bitbucket.org/n-gl/ngl/text"
	"bitbucket.org/tentontrain/math"
)

var sampleText = "I've studied now Philosophy and Jurisprudence, Medicine, and even, alas! Theology."

func main() {
	window, err := sprite.InitializeWindow(600, 600, "fondling", false)
	if err != nil {
		panic(err)
	}

	renderer := sprite.Renderer{}
	renderer.Init()

	ascii := text.CharacterSetASCII()
	greek := text.CharacterSetGreek()
	both := append(ascii, greek...)

	shader, err := shaders.NewFancySpriteShader()
	if err != nil {
		panic(err)
	}

	textMap, err := text.NewTextMap("../data/font.ttf", 32, both)
	if err != nil {
		panic(err)
	}

	store, err := sprite.NewSpriteStore(textMap.FontAtlas.AtlasImage, textMap.ToAtlas(), shader, 128, math.Point{600, 600})
	renderer.AddSpriteStore(store, 1)
	if err != nil {
		panic(err)
	}

	sprites := text.TextToSprites([][]rune{[]rune(sampleText)}, textMap, store, math.Box{}.New(300, 300, 600, 600), 1, 1, 1)
	split := [][]rune{[]rune("Andreas"), []rune(" Ladas")}
	sprites2 := text.TextToSprites(split, textMap, store, math.Box{}.New(0, 300, 300, 450), 1, 1, 1)
	sprites = append(sprites, sprites2...)

	for i := range sprites {
		sprites[i].SetColor(color.COLOR_WHEEL_BASIC[i%len(color.COLOR_WHEEL_BASIC)])
		store.AddSprite(sprites[i])
	}

	start := time.Now().UnixNano()
	for {
		dt := float32((time.Now().UnixNano() - start) / int64(time.Millisecond*100))
		err := store.UpdateUniform("time", []float32{dt})
		if err != nil {
			panic(err)
		}
		//time.Sleep(time.Millisecond * 100)
		renderer.Render()
		window.GLSwap()
	}
}

func Color(r, g, b, a float32) []float32 {
	return []float32{r, g, b, a}
}

func Repeat(data []float32, num int) []float32 {
	r := make([]float32, len(data)*num)
	for i := 0; i < num; i++ {
		for j := 0; j < len(data); j++ {
			r[i*len(data)+j] = data[j]
		}
	}
	return r
}
