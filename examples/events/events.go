package main

import (
	"fmt"
	"os"

	"bitbucket.org/n-gl/ngl/platform"
	"bitbucket.org/n-gl/ngl/sprite"
	"github.com/veandco/go-sdl2/sdl"
)

func main() {
	window, err := platform.InitializeWindow(600, 600, "simple", true)
	if err != nil {
		panic(err)
	}

	renderer := sprite.Renderer{}
	renderer.Init()

	events := platform.EventManager{}

	for {
		events.GatherEvents()
		if kb := events.GetKeyboardEvents(); len(kb) > 0 {
			fmt.Println(kb)
		}

		if events.GetQuitEvent() {
			fmt.Println("Exiting...")
			window.Destroy()
			sdl.Quit()
			os.Exit(0)
		}

		if win := events.GetResizeEvents(); len(win) > 0 {
			fmt.Println(win)
		}

		renderer.Render()
		window.GLSwap()
	}
}
