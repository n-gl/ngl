package main

import (
	"time"

	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/n-gl/ngl/sprite/shaders"
	"bitbucket.org/tentontrain/math"
)

type AnimatedSprite struct {
	store        *sprite.SpriteStore
	frames       int
	currentFrame int
	position     math.Point
}

func (s *AnimatedSprite) Draw(dt int) sprite.Sprite {
	s.currentFrame = (s.currentFrame + 1) % s.frames
	sprite, _ := sprite.NewSprite(s.currentFrame, s.store)
	sprite.SetDepth(1)
	sprite.SetPosition(s.position)
	sprite.SetSize(math.Point{60, 60})
	s.position.X += dt
	if s.position.X > 600 {
		s.position.X = 0
	}
	return sprite
}

func main() {
	window, err := sprite.InitializeWindow(600, 600, "simple", false)
	if err != nil {
		panic(err)
	}

	renderer := sprite.Renderer{}
	renderer.Init()

	shader, err := shaders.NewColorSpriteShader()
	if err != nil {
		panic(err)
	}

	store, err := sprite.NewSpriteStoreFromFiles("../data/pumpkin_dude.png", "../data/pumpkin_dude.atlas",
		shader, 12, math.Point{600, 600})
	if err != nil {
		panic(err)
	}
	renderer.AddSpriteStore(store, 1)
	as := AnimatedSprite{
		store:    store,
		frames:   8,
		position: math.Point{0, 300},
	}

	for {
		store.Empty()
		if err = renderer.AddSprite(as.Draw(10)); err != nil {
			panic(err)
		}

		time.Sleep(time.Millisecond * 100)
		renderer.Render()
		window.GLSwap()
	}
}
