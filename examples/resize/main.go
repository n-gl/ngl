package main

import (
	"fmt"
	"time"

	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/n-gl/ngl/sprite/shaders"
	"bitbucket.org/tentontrain/math"
	"github.com/veandco/go-sdl2/sdl"
)

var text = "I've studied now Philosophy and Jurisprudence, Medicine, and even, alas! Theology."

func main() {
	window, err := sprite.InitializeWindow(600, 600, "fondling", true)
	if err != nil {
		panic(err)
	}

	renderer := sprite.Renderer{}
	renderer.Init()

	ascii := sprite.CharacterSetASCII()
	greek := sprite.CharacterSetGreek()
	both := append(ascii, greek...)

	shader, err := shaders.NewFancySpriteShader()
	if err != nil {
		panic(err)
	}

	store, err := sprite.NewTextStore("font.ttf", shader, 32, both, 512, math.Point{600, 600})
	if err != nil {
		panic(err)
	}
	renderer.AddSpriteStore("a_sprite_layer", &store.SpriteStore, 1)

	start := time.Now().UnixNano()
	for {
		event := sdl.PollEvent()
		switch t := event.(type) {
		case *sdl.WindowEvent:
			if t.Event == sdl.WINDOWEVENT_RESIZED {
				renderer.Resize(int(t.Data1), int(t.Data2))
				fmt.Println(int(t.Data1), int(t.Data2))
			}
		}

		store.Empty()

		// manual character placement
		x := store.FontAtlas.HSpace
		store.AddRune('N', 0, 0, sprite.Depth(1), 1)
		color := Repeat(Color(1, 0, 0, 1), 6)
		store.UpdateBuffers("color", 0, color)
		store.AddRune('g', x, 10, sprite.Depth(1), 1)
		color = Repeat(Color(0, 1, 0, 1), 6)
		store.UpdateBuffers("color", 1, color)
		store.AddRune('λ', 2*x, 20, sprite.Depth(1), 1)
		color = Repeat(Color(0, 0, 1, 1), 6)
		store.UpdateBuffers("color", 2, color)

		// rendering text in a region
		color = Repeat(Color(1, 0, 1, 1), 6*len(text)-60) //spaces are not added as sprites
		store.UpdateBuffers("color", 3, color)
		store.AddText([]rune(text), math.Box{}.New(300, 300, 600, 600), sprite.Depth(1), 1, 1)

		split := [][]rune{[]rune(text[0:10]), []rune(text[10:12])}
		store.AddTextMulti(split, math.Box{}.New(0, 300, 150, 450), sprite.Depth(1), 1, 1)
		color = Repeat(Color(0, 1, 1, 1), 6*12)
		store.UpdateBuffers("color", 3+len(text)-10, color)

		dt := float32((time.Now().UnixNano() - start) / int64(time.Millisecond*100))
		//fmt.Println(dt)
		store.UpdateUniform("time", []float32{dt})
		//time.Sleep(time.Millisecond * 100)
		renderer.Render()
		window.GLSwap()
	}
}

func Color(r, g, b, a float32) []float32 {
	return []float32{r, g, b, a}
}

func Repeat(data []float32, num int) []float32 {
	r := make([]float32, len(data)*num)
	for i := 0; i < num; i++ {
		for j := 0; j < len(data); j++ {
			r[i*len(data)+j] = data[j]
		}
	}
	return r
}
