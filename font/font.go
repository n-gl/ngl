// Renders a truetype font to an image and stores bounding boxes for each rune in map
package font

import (
	"fmt"
	"image"
	"image/draw"
	"io/ioutil"

	"bitbucket.org/tentontrain/math"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
	"golang.org/x/image/math/fixed"
)

// GlyphInfo holds rendering information about a glyph
type GlyphInfo struct {
	BBox    math.Box2D[int]
	Origin  math.Vector2[int]
	VAdjust int
	HAdjust int
}

// Atlas holds an atlas image and the required per-glyph info to render each glyph
// You must call Atlas.Construct before using the atlas
type FontAtlas struct {
	AtlasImage     *image.RGBA        // atlas image
	Width, Height  int                // atlas image dimensions
	Glyphs         map[rune]GlyphInfo // render info per glyph
	HSpace, VSpace int                // spacing between chars/lines
}

// Produce an image of a given font that contains bitmapped representations of
// the characters in charSet.
func (a *FontAtlas) Init(fontFile string, fontSize float64, charSet []rune) error {
	// Read the font data.
	fontBytes, err := ioutil.ReadFile(fontFile)
	if err != nil {
		return err
	}
	f, err := truetype.Parse(fontBytes)
	if err != nil {
		return err
	}
	face := truetype.NewFace(f, &truetype.Options{
		Size:    fontSize,
		Hinting: font.HintingNone, // TODO make this a param ?
	})

	// todo magic numbers
	a.Width = 1080
	a.Height = 1080
	_, spacesdv, _ := face.GlyphBounds(rune(' '))
	a.HSpace = spacesdv.Ceil()

	a.VSpace = face.Metrics().Height.Ceil()

	a.Glyphs = make(map[rune]GlyphInfo)
	// todo variable sized atlas image
	atlasImg := image.NewRGBA(image.Rect(0, 0, a.Width-1, a.Height-1))

	yStep := face.Metrics().Ascent + face.Metrics().Descent
	dot := fixed.P(0, face.Metrics().Ascent.Ceil())

	for _, ru := range charSet {
		// retrieve glyph parametrs
		bounds, advance, ok := face.GlyphBounds(ru)
		if !ok {
			// missing rune
			fmt.Println("Missing rune:", ru)
			continue
		}

		dr, mask, maskp, _, _ := face.Glyph(dot, ru)
		draw.Draw(atlasImg, dr, mask, maskp, draw.Src)

		a.Glyphs[ru] = GlyphInfo{
			BBox: math.Box2D[int]{
				P1: math.Vector2[int]{dr.Min.X, dr.Min.Y},
				P2: math.Vector2[int]{dr.Max.X, dr.Max.Y},
			},
			Origin:  math.Vector2[int]{dot.X.Floor(), dot.Y.Floor()},
			VAdjust: bounds.Max.Y.Ceil(), // but why ?
			HAdjust: 0,
		}

		dot.X += advance

		// new line?
		if dot.X+advance > fixed.I(a.Width) {
			dot.Y += yStep
			dot.X = 0
		}
	}

	a.AtlasImage = atlasImg
	return nil
}

func (a *FontAtlas) ToFile(imageFile string) {
	RgbaToPng(a.AtlasImage, imageFile)
}
