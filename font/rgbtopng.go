package font

import (
	"bufio"
	"image"
	"image/png"
	"os"
)

// Save rgba image to disk. Mostly for testing
func RgbaToPng(image *image.RGBA, filename string) error {
	outFile, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer outFile.Close()

	b := bufio.NewWriter(outFile)

	if err := png.Encode(b, image); err != nil {
		return err
	}
	if err = b.Flush(); err != nil {
		return err
	}

	return nil
}
