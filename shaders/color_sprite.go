package shaders

//go:generate go run shader-gen.go -source source/color_sprite.vertex -package shaders -var ColorSpriteVertex -out color_sprite_vertex_gen.go
//go:generate go run shader-gen.go -source source/color_sprite.fragment -package shaders -var ColorSpriteFragment -out color_sprite_fragmet_gen.go

func NewColorSpriteShader() (Shader, error) {
	attributes := map[string]ShaderAttribute{
		"vertex": {
			Name:     "vertex",
			Location: 0,
			Type:     GLSLType{"vec3", 3},
		},
		"uv": {
			Name:     "uv",
			Location: 1,
			Type:     GLSLType{"vec2", 2},
		},
		"color": {
			Name:     "color",
			Location: 2,
			Type:     GLSLType{"vec4", 4},
			Default:  []float32{1, 1, 1, 1},
		},
		"transform": {
			Name:     "transform",
			Location: 3,
			Type:     GLSLType{"vec3", 3},
			Default:  []float32{0, 0, 0},
		},
		"scale": {
			Name:     "scale",
			Location: 4,
			Type:     GLSLType{"vec2", 2},
			Default:  []float32{1, 1},
		},
	}

	uniforms := map[string]GLSLType{
		"matrix": {"mat4", 16},
	}
	shader, err := NewShader(ColorSpriteVertex, ColorSpriteFragment, attributes, uniforms)
	if err != nil {
		return shader, err
	}
	return shader, nil
}
