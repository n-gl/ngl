package shaders
var ColorSpriteVertex = `#version 410

layout (location=0) in vec3 vertex; // vertex position
layout (location=1) in vec2 uv; // per-vertex texture co-ords
layout (location=2) in vec4 color; 
layout (location=3) in vec3 transform;
layout (location=4) in vec2 scale;

uniform  mat4 matrix;

out vec2 texCoords;
out vec4 vertColor;

void main() {
    texCoords = uv;

    mat4 scale_translate_mat = mat4(1.0);
    scale_translate_mat[0][0] = scale.x;
    scale_translate_mat[1][1] = scale.y;
    scale_translate_mat[3] = vec4(transform, 1);
    
    gl_Position = matrix  * scale_translate_mat * vec4(vertex, 1.0); 
    vertColor = color;
}




 ` + "\x00" 
