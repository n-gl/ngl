package shaders

var ColorSpriteFragment = `#version 410

uniform sampler2D tex;

in vec2 texCoords;
in vec4 vertColor;
out vec4 frag_colour;

void main() {
    //frag_colour = texture(tex, texCoords) * vertColor ;
    frag_colour = texture(tex, texCoords) * vertColor ;
} 

 ` + "\x00"
