// OpenGL shader and program compilation utils
package shaders

import (
	"errors"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/go-gl/gl/v4.6-core/gl"
)

// Shader stores a GLSL vertex and fragment shader pair and provides easy access to shader attributes
type Shader struct {
	Vertex, Fragment string                     // shader source
	Program          uint32                     // opengl program
	Attributes       map[string]ShaderAttribute // named access to shader attributes - the name must match the shader source
	Uniforms         map[string]GLSLType        // named access to shader unifrom parameters - the name must match the shader source
}

// Create a ShaderAttributeData which matches the requirements of this shader
func (s Shader) AttributeData() map[string]ShaderAttribute {
	data := make(map[string]ShaderAttribute)
	for k, v := range s.Attributes {
		data[k] = v
	}
	return data
}

// NewShader creates a Shader with given sources and attributes. A simple sanity check is performed which checks that the attribute
// name and type are present in the shader source.
func NewShader(vertexShader, fragmentShader string, attributes map[string]ShaderAttribute,
	uniforms map[string]GLSLType) (Shader, error) {

	shader := Shader{}
	var err error
	if shader.Program, err = createGLProgram(vertexShader, fragmentShader); err != nil {
		return shader, err
	}

	shader.Attributes = attributes
	shader.Uniforms = uniforms
	shader.Vertex = vertexShader
	shader.Fragment = fragmentShader

	shaderLines := strings.SplitAfter(vertexShader, "\n")
	for name, attr := range attributes {
		found := false
		for _, line := range shaderLines {
			if strings.Contains(line, name) && strings.Contains(line, attr.Type.Name) {
				found = true
				break
			}
		}
		if !found {
			return shader, errors.New(fmt.Sprint("Attribute not found: ", name, attr.Type))
		}
	}

	// uniforms can be in both shaders
	shaderLines = append(shaderLines, strings.SplitAfter(fragmentShader, "\n")...)
	for name, gltype := range uniforms {
		found := false
		for _, line := range shaderLines {
			if strings.Contains(line, name) && strings.Contains(line, gltype.Name) {
				found = true
				break
			}
		}
		if !found {
			return shader, errors.New(fmt.Sprintf("Uniform not found: %v %v", name, gltype.Name))
		}
	}

	return shader, nil
}

// Same as NewShader but shaders are loaded from files
func NewShaderFromFiles(vertexShaderFilename, fragmentShaderFilename string,
	attributes map[string]ShaderAttribute, uniforms map[string]GLSLType) (Shader, error) {

	vertSource, err := shaderSourceFromFile(vertexShaderFilename)
	if err != nil {
		return Shader{}, err
	}
	fragSource, err := shaderSourceFromFile(fragmentShaderFilename)
	if err != nil {
		return Shader{}, err
	}

	return NewShader(vertSource, fragSource, attributes, uniforms)
}

// Create an opengl program
func createGLProgram(vertexShader, fragmentShader string) (uint32, error) {
	var err error
	var VertexShader, FragmentShader uint32
	if VertexShader, err = compileShader(vertexShader, gl.VERTEX_SHADER); err != nil {
		return 0, err
	}
	if FragmentShader, err = compileShader(fragmentShader, gl.FRAGMENT_SHADER); err != nil {
		return 0, err
	}

	prog := gl.CreateProgram()
	gl.AttachShader(prog, VertexShader)
	gl.AttachShader(prog, FragmentShader)
	gl.LinkProgram(prog)

	return prog, nil
}

// compileShader given in source. shaderType can be gl.VERTEX_SHADER / gl.FRAGMENT_SHADER
func compileShader(source string, shaderType uint32) (uint32, error) {
	shader := gl.CreateShader(shaderType)

	csources, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to compile %v: %v", source, log)
	}

	return shader, nil
}

// helper to load shaders from file
func shaderSourceFromFile(filename string) (string, error) {
	bytes, err := ioutil.ReadFile(filename)

	if err != nil {
		return "", err
	}
	return string(bytes) + "\x00", nil
}
