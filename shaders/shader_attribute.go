package shaders

// Stores the location index and type of an attribute in the shader
type ShaderAttribute struct {
	Name     string    // Attribute name as it appears in the shader source
	Location uint32    // OpenGL attribute index - assigned with EnableVertexAttribArray
	Type     GLSLType  // GLSL type (vec2, vec3 etc)
	Default  []float32 // default value for this attribute
	Data     []float32 // data of this attribute - its size should be Type.Size * vertices
}
