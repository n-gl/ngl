package text

import (
	"bitbucket.org/n-gl/ngl/font"
	"bitbucket.org/n-gl/ngl/sprite"
)

type TextMap struct {
	RuneMap   map[rune]sprite.SpriteID // mapping of characters to SpriteStore indices
	FontAtlas font.FontAtlas           // font-specific info
}

func NewTextMap(fontFile string, fontSize float64, charSet []rune) (*TextMap, error) {
	fa := font.FontAtlas{}
	if err := fa.Init(fontFile, fontSize, charSet); err != nil {
		return nil, err
	}

	return &TextMap{
		RuneMap:   map[rune]sprite.SpriteID{},
		FontAtlas: fa,
	}, nil
}

// Create an atlas map for sprite.SpriteStore
func (tm *TextMap) ToAtlas() sprite.AtlasMap {
	aMap := sprite.AtlasMap{}
	aMap.Width, aMap.Height = tm.FontAtlas.Width, tm.FontAtlas.Height
	for k, v := range tm.FontAtlas.Glyphs {
		index := aMap.AddSprite(v.BBox)
		tm.RuneMap[k] = sprite.SpriteID(index)
	}
	return aMap
}
