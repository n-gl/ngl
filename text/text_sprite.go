package text

import (
	"errors"

	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/tentontrain/math"
)

func NewTextSprite(r rune, textMap *TextMap, spriteStore *sprite.SpriteStore) (sprite.Sprite, error) {
	spriteIndex, ok := textMap.RuneMap[r]
	if !ok {
		return sprite.Sprite{}, errors.New("No such rune in store")
	}
	sprits, _ := sprite.NewSprite(spriteIndex, spriteStore)
	bb := textMap.FontAtlas.Glyphs[r].BBox
	sprits.SetSize(bb.Size())

	return sprits, nil
}

func TextToSprites(text [][]rune, textMap *TextMap, spriteStore *sprite.SpriteStore,
	box math.Box2D[int], z, tabWidth int, scale float64) []sprite.Sprite {

	sprites := []sprite.Sprite{}
	hSpace := textMap.FontAtlas.HSpace
	vSpace := textMap.FontAtlas.VSpace

	// drawing point starts at the top left of the window
	dp := math.Vector2[int]{box.P1.X, box.P2.Y - vSpace}
	chars := 0

	for _, slice := range text {
		overshot := false
		for _, c := range slice {
			switch c {
			case '\n':
				dp.Y -= vSpace
				dp.X = 0
			case ' ':
				dp.X += hSpace
			case '\t':
				dp.X += hSpace * tabWidth
			default:
				// per-glyph adjustment
				x, y := dp.X-textMap.FontAtlas.Glyphs[c].HAdjust, dp.Y-textMap.FontAtlas.Glyphs[c].VAdjust
				if _, ok := textMap.RuneMap[c]; ok {
					//ts.AddRune(c, x, y, z, scale)
					sprits, _ := NewTextSprite(c, textMap, spriteStore)
					sprits.SetPosition(math.Vector2[int]{x, y})
					sprites = append(sprites, sprits)
					dp.X += hSpace
				}
			}
			chars++
			// wrap if we went past the rightmost edge of the box
			if dp.X+vSpace >= box.P2.X {
				dp.Y -= vSpace
				dp.X = box.P1.X
			}
			// stop if we overshot the bottom end of the box
			if dp.Y < box.P1.Y {
				overshot = true
				chars--
				break
			}
		}
		if overshot {
			break
		}
	}
	return sprites
}
