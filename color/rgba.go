package color

import "bitbucket.org/tentontrain/math"

// Holds red, green, blue, alpha values. Each should be clamped in [0,1]. A=1 is fully opaque.
type ColorRGBA struct {
	R, G, B, A float32
}

func NewColorRGBA(r, g, b, a float32) ColorRGBA {
	return ColorRGBA{
		R: r, G: g, B: b, A: a,
	}
}

func NewColorRGBAFromArray(color [4]float32) ColorRGBA {
	return ColorRGBA{
		R: color[0], G: color[1], B: color[2], A: color[3],
	}
}

func NewColorRGBAFromSlice(color []float32) ColorRGBA {
	if len(color) != 4 {
		return COLOR_MAGENDA
	}

	return ColorRGBA{
		R: color[0], G: color[1], B: color[2], A: color[3],
	}
}

func NewColorRGBAFromBytes(r, g, b, a uint8) ColorRGBA {
	return ColorRGBA{
		R: float32(r) / 255,
		G: float32(g) / 255,
		B: float32(b) / 255,
		A: float32(a) / 255,
	}
}

// Make sure each component is in [0,1]
func (c *ColorRGBA) Clamp() {
	c.R = math.Bound(c.R, 0, 1)
	c.G = math.Bound(c.G, 0, 1)
	c.B = math.Bound(c.B, 0, 1)
	c.A = math.Bound(c.A, 0, 1)
}

func (c *ColorRGBA) ToArray() [4]float32 {
	return [4]float32{c.R, c.G, c.B, c.A}
}
