package ui

import (
	"reflect"
	"testing"

	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/tentontrain/math"
)

func TestDrawTree(t *testing.T) {
	main := &MainWindow{}
	im1 := NewImage(1, sprite.SpriteStoreID(1), math.Point{0, 0}, 1)
	im2 := NewImage(2, sprite.SpriteStoreID(1), math.Point{0, 0}, 1)
	im11 := NewImage(11, sprite.SpriteStoreID(1), math.Point{0, 0}, 1)
	im12 := NewImage(12, sprite.SpriteStoreID(1), math.Point{0, 0}, 1)
	im21 := NewImage(21, sprite.SpriteStoreID(1), math.Point{0, 0}, 1)
	im211 := NewImage(211, sprite.SpriteStoreID(1), math.Point{0, 0}, 1)
	expectedIds := []int{-1, 1, 11, 12, 2, 21, 211}

	main.AddChild(im1)
	main.AddChild(im2)
	im1.AddChild(im11)
	im1.AddChild(im12)
	im2.AddChild(im21)
	im21.AddChild(im211)

	sprites := DrawTree(main)
	ids := []int{}
	for _, v := range sprites {
		ids = append(ids, v.Id)
	}

	if !reflect.DeepEqual(ids, expectedIds) {
		t.Error(ids, "\n", expectedIds)
	}

}
