package ui

import (
	"bitbucket.org/n-gl/ngl/sprite"
)

// Draws everything in the tree whose root is r
func DrawTree(r Element) []sprite.Sprite {
	sprites := []sprite.Sprite{}
	sprites = drawNode(r, sprites)
	return sprites
}

func drawNode(n Element, sprites []sprite.Sprite) []sprite.Sprite {
	sprites = append(sprites, n.Draw()...)
	for _, v := range n.GetChildren() {
		sprites = drawNode(v, sprites)
	}
	return sprites
}
