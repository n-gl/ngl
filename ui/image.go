package ui

import (
	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/tentontrain/math"
)

// Image draws a sprite which fills it's parent's bounding box.

type Image struct {
	sprite sprite.Sprite
	store  *sprite.SpriteStore
	UI
}

func NewImage(spriteId sprite.SpriteID, spriteStore *sprite.SpriteStore) *Image {

	sprite, err := sprite.NewSprite(spriteId, spriteStore)
	if err != nil {
		panic(err)
	}

	img := Image{
		store:  spriteStore,
		sprite: sprite,
	}

	return &img
}

func (a *Image) Update(depth int) {
	a.UI.BoundingBox = a.Parent.BoundingBox()
	a.sprite.SetPosition(a.UI.BoundingBox.P1)
	a.sprite.SetDepth(depth - 1)
	a.sprite.SetSize(a.UI.BoundingBox.Size())
	for _, c := range a.Children {
		c.Update(depth - 1)
	}
}

func (a *Image) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *Image) SetParent(parent Element) {
	a.Parent = parent
}

func (a *Image) BoundingBox() math.Box2D[int] {
	return a.UI.BoundingBox
}

func (a *Image) GetChildren() []Element {
	return a.Children
}

func (a *Image) Draw() []sprite.Sprite {
	return []sprite.Sprite{a.sprite}
}
