package ui

import (
	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/tentontrain/math"
)

// Slits the parent vertically starting at Start% ant ending at End%
type VerticalSplitBox struct {
	Start, End int // Where to start and end the split (0-100%)
	UI
}

func (a *VerticalSplitBox) Update(depth int) {
	parentBB := a.Parent.BoundingBox()
	parentSize := parentBB.Size()

	a.UI.BoundingBox.P1.X = parentBB.P1.X + a.Start*parentSize.X/100
	a.UI.BoundingBox.P2.X = parentBB.P1.X + a.End*parentSize.X/100
	a.UI.BoundingBox.P1.Y = parentBB.P1.Y
	a.UI.BoundingBox.P2.Y = parentBB.P2.Y

	for _, c := range a.Children {
		c.Update(depth - 1)
	}
}

func (a *VerticalSplitBox) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *VerticalSplitBox) SetParent(parent Element) {
	a.Parent = parent
}

func (a *VerticalSplitBox) BoundingBox() math.Box2D[int] {
	return a.UI.BoundingBox
}

func (a *VerticalSplitBox) GetChildren() []Element {
	return a.Children
}

func (a *VerticalSplitBox) Draw() []sprite.Sprite {
	return []sprite.Sprite{sprite.Sprite{Id: sprite.NoopSprite}}
}
