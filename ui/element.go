package ui

import (
	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/tentontrain/math"
)

// Element is the base interface for UI elements.
type Element interface {
	Update(depth int)             // determines the ui's position and size which can be relevant to its parent
	AddChild(child Element)       // add a Element as child - must also set itself as the child's parent
	SetParent(parent Element)     // set this Element parent
	BoundingBox() math.Box2D[int] // get the Element's BoundingBox
	GetChildren() []Element       // returns the Element's children nodes

	sprite.SpriteDrawer // UI elements are drawable using sprites
}

// Common data for UI elements
type UI struct {
	ID          uint
	Children    []Element
	Parent      Element
	BoundingBox math.Box2D[int]
}
