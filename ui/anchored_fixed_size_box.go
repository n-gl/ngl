package ui

import (
	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/tentontrain/math"
)

// AnchoredFixedSizeBox anchors to a relative point on it's parent's bounding box. The relative point is given as a percentile
// of the parent's bounding box. This allows the anchor to reposition when the parent resizes.
// Examples:
// 0,0 anchors to the lower left corner
// 0,100 anchors to the upper left corner
// 50,50 anchors to the middle of the parent's box
// The box can be further adjusted using an offset given in pixels
type AnchoredFixedSizeBox struct {
	AnchorPercentage math.Vector2[int]
	AnchorOffset     math.Vector2[int]
	Size             math.Vector2[int]
	UI
}

func NewAnchoredFixedSizeBox(anchorPercentage, anchorOffset, size math.Vector2[int]) *AnchoredFixedSizeBox {
	return &AnchoredFixedSizeBox{
		AnchorPercentage: anchorPercentage,
		AnchorOffset:     anchorOffset,
		Size:             size,
	}
}

func (a *AnchoredFixedSizeBox) Update(depth int) {
	a.AnchorPercentage.X = math.Bound(a.AnchorPercentage.X, 0, 100)
	a.AnchorPercentage.Y = math.Bound(a.AnchorPercentage.Y, 0, 100)
	parentBB := a.Parent.BoundingBox()
	parentSize := parentBB.Size()
	parentOffset := parentSize.Mul(a.AnchorPercentage).Div(math.Vector2[int]{100, 100})
	a.UI.BoundingBox.P1 = a.Parent.BoundingBox().P1.Add(parentOffset).Add(a.AnchorOffset)
	a.UI.BoundingBox.P2 = a.UI.BoundingBox.P1.Add(a.Size)

	for _, c := range a.Children {
		c.Update(depth - 1)
	}
}

func (a *AnchoredFixedSizeBox) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *AnchoredFixedSizeBox) SetParent(parent Element) {
	a.Parent = parent
}

func (a *AnchoredFixedSizeBox) BoundingBox() math.Box2D[int] {
	return a.UI.BoundingBox
}

func (a *AnchoredFixedSizeBox) GetChildren() []Element {
	return a.Children
}

func (a *AnchoredFixedSizeBox) Draw() []sprite.Sprite {
	return []sprite.Sprite{sprite.Sprite{Id: sprite.NoopSprite}}
}
