package ui

import (
	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/tentontrain/math"
)

// Slits the parent horizontaly starting at Start% ant ending at End%
type HorizontalSplitBox struct {
	Start, End int // Where to start and end the split (0-100%)
	UI
}

func (a *HorizontalSplitBox) Update(depth int) {
	parentBB := a.Parent.BoundingBox()
	parentSize := parentBB.Size()

	a.UI.BoundingBox.P1.Y = parentBB.P1.Y + a.Start*parentSize.Y/100
	a.UI.BoundingBox.P2.Y = parentBB.P1.Y + a.End*parentSize.Y/100
	a.UI.BoundingBox.P1.X = parentBB.P1.X
	a.UI.BoundingBox.P2.X = parentBB.P2.X

	for _, c := range a.Children {
		c.Update(depth - 1)
	}
}

func (a *HorizontalSplitBox) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *HorizontalSplitBox) SetParent(parent Element) {
	a.Parent = parent
}

func (a *HorizontalSplitBox) BoundingBox() math.Box2D[int] {
	return a.UI.BoundingBox
}

func (a *HorizontalSplitBox) GetChildren() []Element {
	return a.Children
}

func (a *HorizontalSplitBox) Draw() []sprite.Sprite {
	return []sprite.Sprite{sprite.Sprite{Id: sprite.NoopSprite}}
}
