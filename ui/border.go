package ui

import (
	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/tentontrain/math"
)

// Draws a border around the parent bounding box
type Border struct {
	//corners
	ll, lr, ul, ur sprite.Sprite
	//top/bottom/left/right
	t, b, l, r sprite.Sprite
	store      *sprite.SpriteStore
	UI
}

// Provide sprite ids for four corners and vertical/horizontal sections
func NewBorder(lowerLeft, lowerRight, upperLeft, upperRight, left, right, top, bot sprite.SpriteID,
	store *sprite.SpriteStore) *Border {

	b := &Border{store: store}

	b.ll, _ = sprite.NewSprite(lowerLeft, store)
	b.lr, _ = sprite.NewSprite(lowerRight, store)
	b.ul, _ = sprite.NewSprite(upperLeft, store)
	b.ur, _ = sprite.NewSprite(upperRight, store)
	b.l, _ = sprite.NewSprite(left, store)
	b.r, _ = sprite.NewSprite(right, store)
	b.t, _ = sprite.NewSprite(top, store)
	b.b, _ = sprite.NewSprite(bot, store)
	return b
}

func (a *Border) Update(depth int) {
	bb := a.Parent.BoundingBox()
	a.UI.BoundingBox = bb

	ss := a.ll.GetOriginalSize()

	a.ll.SetPosition(bb.P1)
	a.lr.SetPosition(math.Vector2[int]{X: bb.P2.X - ss.X, Y: bb.P1.Y})
	a.ur.SetPosition(bb.P2.Sub(ss))
	a.ul.SetPosition(math.Vector2[int]{X: bb.P1.X, Y: bb.P2.Y - ss.Y})

	a.l.SetPosition(math.Vector2[int]{X: bb.P1.X, Y: bb.P1.Y + ss.Y})
	a.r.SetPosition(math.Vector2[int]{X: bb.P2.X - ss.X, Y: bb.P1.Y + ss.Y})
	a.t.SetPosition(math.Vector2[int]{X: bb.P1.X + ss.X, Y: bb.P2.Y - ss.Y})
	a.b.SetPosition(math.Vector2[int]{X: bb.P1.X + ss.X, Y: bb.P1.Y})

	a.ll.SetOriginalSize()
	a.lr.SetOriginalSize()
	a.ul.SetOriginalSize()
	a.ur.SetOriginalSize()

	a.t.SetSize(math.Vector2[int]{X: bb.Size().X - 2*ss.X, Y: ss.Y})
	a.b.SetSize(math.Vector2[int]{X: bb.Size().X - 2*ss.X, Y: ss.Y})
	a.l.SetSize(math.Vector2[int]{X: ss.X, Y: bb.Size().Y - 2*ss.Y})
	a.r.SetSize(math.Vector2[int]{X: ss.X, Y: bb.Size().Y - 2*ss.Y})

	for _, c := range a.Children {
		c.Update(depth - 1)
	}
}

func (a *Border) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *Border) SetParent(parent Element) {
	a.Parent = parent
}

func (a *Border) BoundingBox() math.Box2D[int] {
	// report a smaller bounding box to account for the border width
	bb := a.UI.BoundingBox
	width := a.ll.GetOriginalSize()
	bb.P1 = bb.P1.Add(width)
	bb.P2 = bb.P2.Sub(width)
	return bb
}

func (a *Border) GetChildren() []Element {
	return a.Children
}

func (a *Border) Draw() []sprite.Sprite {
	return []sprite.Sprite{a.ll, a.lr, a.ul, a.ur, a.l, a.r, a.t, a.b}
}
