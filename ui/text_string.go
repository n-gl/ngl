package ui

import (
	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/n-gl/ngl/text"
	"bitbucket.org/tentontrain/math"
)

type TextString struct {
	Text string
	UI

	textMap     *text.TextMap
	spriteStore *sprite.SpriteStore
	sprites     []sprite.Sprite
}

func NewTextString(text string, textMap *text.TextMap, spriteStore *sprite.SpriteStore) *TextString {
	return &TextString{
		Text:        text,
		textMap:     textMap,
		spriteStore: spriteStore,
	}
}

func (a *TextString) Update(depth int) {
	a.sprites = text.TextToSprites(
		[][]rune{[]rune(a.Text)},
		a.textMap,
		a.spriteStore,
		a.UI.Parent.BoundingBox(),
		depth-1, 4, 1)

	for _, c := range a.Children {
		c.Update(depth - 1)
	}
}

func (a *TextString) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *TextString) SetParent(parent Element) {
	a.Parent = parent
}

func (a *TextString) BoundingBox() math.Box2D[int] {
	return a.UI.BoundingBox
}

func (a *TextString) GetChildren() []Element {
	return a.Children
}

func (a *TextString) Draw() []sprite.Sprite {
	return a.sprites
}
