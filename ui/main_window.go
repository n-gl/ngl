package ui

import (
	"bitbucket.org/n-gl/ngl/sprite"
	"bitbucket.org/tentontrain/math"
)

// Holds the entire UI. It's boundong box should be kept up to date with the app window's size
type MainWindow struct{ UI }

func (a *MainWindow) Update(depth int) {
	// no parrent, so assumes its own bounding box has been set manually
	// update all children
	for _, c := range a.Children {
		c.Update(sprite.MAX_DEPTH)
	}
}

func (a *MainWindow) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *MainWindow) SetParent(parent Element) {
	panic("Top level window has no parent")
}

func (a *MainWindow) BoundingBox() math.Box2D[int] {
	return a.UI.BoundingBox
}

func (a *MainWindow) GetChildren() []Element {
	return a.Children
}

func (a *MainWindow) Draw() []sprite.Sprite {
	// placeholder ui doesn't draw
	return []sprite.Sprite{sprite.Sprite{Id: sprite.NoopSprite}}
}
