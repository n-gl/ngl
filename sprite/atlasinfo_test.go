package sprite

import (
	"testing"

	"bitbucket.org/tentontrain/math"
)

func TestDumpExampleAtlas(t *testing.T) {
	a := AtlasMap{}
	a.Width = 128
	a.Height = 32
	a.AddSprite(math.Box{
		math.Point{0, 11},
		math.Point{14, 31},
	})
	a.MapSprites("a_sprite", math.Point{0, 0})
	marshalFile(a, "test.atlas", true)
}
