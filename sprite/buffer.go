// Functions for creating and updating opengl buffers
package sprite

import (
	"errors"

	"github.com/go-gl/gl/v4.6-core/gl"
)

// Create two VBOs for holding vertex and uvs and a VAO to tie them together
func makeGLQuadBuffers(size int) (vertexVBO uint32, uvVBO uint32, vao uint32) {

	// VBO to hold vertex data
	gl.GenBuffers(1, &vertexVBO)
	gl.BindBuffer(gl.ARRAY_BUFFER, vertexVBO)
	gl.BufferData(gl.ARRAY_BUFFER, 4*18*size, gl.Ptr(nil), gl.DYNAMIC_DRAW)

	// VBO to hold texture coordinates
	gl.GenBuffers(1, &uvVBO)
	gl.BindBuffer(gl.ARRAY_BUFFER, uvVBO)
	gl.BufferData(gl.ARRAY_BUFFER, 4*12*size, gl.Ptr(nil), gl.DYNAMIC_DRAW)

	// vao combines multiple buffers
	gl.GenVertexArrays(1, &vao)
	gl.BindVertexArray(vao)

	gl.BindBuffer(gl.ARRAY_BUFFER, vertexVBO)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 0, nil)
	gl.EnableVertexAttribArray(0)

	gl.BindBuffer(gl.ARRAY_BUFFER, uvVBO)
	gl.VertexAttribPointer(1, 2, gl.FLOAT, false, 0, nil)
	gl.EnableVertexAttribArray(1)

	return
}

// Allocate a buffer object (VBO in OpenGL parlance) - TODO error checks
func newGLBuffer(size int) uint32 {
	var vbo uint32
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, 4*size, gl.Ptr(nil), gl.DYNAMIC_DRAW) // 4 bytes in a float32
	return vbo
}

// Create a vertex array object (VAO) that binds multiple VBOs
func newVAO(vbos []uint32, sizes []int32, locations []uint32) (uint32, error) {

	if len(vbos) != len(sizes) || len(vbos) != len(locations) {
		return 0, errors.New("len(vbos)!=len(sizes)")
	}

	var vao uint32
	gl.GenVertexArrays(1, &vao)
	gl.BindVertexArray(vao)

	for i := range vbos {
		//fmt.Printf("VAO %v %v %v \n", vbos[i], sizes[i], locations[i])
		gl.BindBuffer(gl.ARRAY_BUFFER, vbos[i])
		gl.VertexAttribPointer(locations[i], sizes[i], gl.FLOAT, false, 0, nil)
		gl.EnableVertexAttribArray(locations[i])
	}

	return vao, nil
}

// Move data to GL buffer
func updateGLBuffer(glbuffer uint32, newData []float32, position int) {
	gl.BindBuffer(gl.ARRAY_BUFFER, glbuffer)
	gl.BufferSubData(gl.ARRAY_BUFFER, 4*position, 4*len(newData), gl.Ptr(newData))
}
