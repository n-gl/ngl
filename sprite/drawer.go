package sprite

// Drawer creates Sprites to be drawn using a SpriteStore
type SpriteDrawer interface {
	Draw() []Sprite
}
