// Atlas holds the coordinates of sprites in an atlas image
package sprite

import (
	"bitbucket.org/tentontrain/math"
)

// Contains a list of bounding boxes that describe how the atlas is split into sprites.
// The order of sprites is important. For example, sprites belonging to an animation
// sequence should be placed in a contiguous chunk so that the animation can
// be descibed a slice of the Sprites array.
// The Map field stores named indexes into the sprites array. Examples:
// Map["bob"] = {4,4} // single sprite
// Map["bob_walk"] = {6,10} // a 5-frame animation
type AtlasMap struct {
	Width, Height int
	Sprites       []math.Box2D[int]
	Map           map[string]math.Vector2[int]
}

// Adds the sprite to the atlas and returns the index at which it was added
func (a *AtlasMap) AddSprite(box math.Box2D[int]) int {
	a.Sprites = append(a.Sprites, box)
	return len(a.Sprites) - 1
}

// Map a sprite range to a given name. Will overwrite existing entries
func (a *AtlasMap) MapSprites(name string, sprites math.Vector2[int]) {
	if a.Map == nil {
		a.Map = make(map[string]math.Vector2[int])
	}
	a.Map[name] = sprites
}

func (ai *AtlasMap) FromFile(filename string) error {
	if err := unmarshalFile(ai, filename); err != nil {
		return err
	}
	return nil
}
