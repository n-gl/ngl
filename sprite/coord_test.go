package sprite

import "testing"

func TestPixelToGl(t *testing.T) {
	tests := []struct {
		x, y, W, H int
		glx, gly   float32
	}{
		{0, 0, 100, 200, -1, -1},
		{50, 100, 100, 200, 0, 0},
	}

	for _, v := range tests {
		glx, gly := pixelToGl(v.x, v.y, v.W, v.H)
		if glx != v.glx || gly != v.gly {
			t.Error(glx, gly, v)
		}
	}
}
