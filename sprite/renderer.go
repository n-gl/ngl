package sprite

import (
	"bitbucket.org/tentontrain/math"
	"github.com/go-gl/gl/v4.6-core/gl"
)

// A renderer holds a collection of sprite stores and displays them
type Renderer struct {
	SpriteStores map[SpriteStoreID]*SpriteStore
	renderOrder  []SpriteStoreID
	bgColor      [4]float32
}

type SpriteStoreID int

func NewRenderer() Renderer {
	return Renderer{SpriteStores: make(map[SpriteStoreID]*SpriteStore)}
}

// Initialize rendering including opengl. Needs active opengl context (e.g. sdl.GLCreateContext)
// TODO: Deal with a opengl being already initialized (if the package is importerd in another rendering system)
func (r *Renderer) Init() {
	if err := gl.Init(); err != nil {
		panic(err)
	}

	if r.SpriteStores == nil {
		r.SpriteStores = make(map[SpriteStoreID]*SpriteStore)
	}

	r.bgColor = [4]float32{0.1, 0.1, 0.1, 1}

	// enable alpha blending (transparency) using the texture's alpha chanel
	gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
	gl.Enable(gl.BLEND)
	gl.Enable(gl.DEPTH_TEST)
}

// Add sprite store to the renderer. Param renderOrder is important for transparent sprites,
// otherwise each sprite's Z coordinate handles the back-front order
func (r *Renderer) AddSpriteStore(store *SpriteStore, order int) {
	r.SpriteStores[store.ID] = store

	if order < 0 {
		order = 0
	}
	if order > len(r.renderOrder) {
		order = len(r.renderOrder)
	}

	// add at the end
	if order == len(r.renderOrder) {
		r.renderOrder = append(r.renderOrder, store.ID)
		return
	}
	// insert
	r.renderOrder = append(r.renderOrder[:order+1], r.renderOrder[order:]...)
	r.renderOrder[order] = store.ID
}

func (r *Renderer) SetBGColor(rgba [4]float32) {
	r.bgColor = rgba
}

func (r *Renderer) Render() {
	// background
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	gl.ClearColor(r.bgColor[0], r.bgColor[1], r.bgColor[2], r.bgColor[3])

	// render every sprite store in RenderOrder
	for _, v := range r.renderOrder {
		r.SpriteStores[v].render()
	}
}

// Resize the render area. Called after the render window has been resized
func (r *Renderer) Resize(w, h int) {
	for _, store := range r.SpriteStores {
		store.window = math.Vector2[int]{w, h}
	}
	gl.Viewport(0, 0, int32(w), int32(h))
}

// Add sprite to be rendered
func (r *Renderer) AddSprite(sprite Sprite) error {
	if sprite.Id == NoopSprite {
		return nil
	}

	store := sprite.SpriteStore

	return store.AddSprite(sprite)
}

func (r *Renderer) AddSprites(sprites []Sprite) error {
	for _, v := range sprites {
		err := r.AddSprite(v)
		if err != nil {
			return err
		}
	}
	return nil
}
