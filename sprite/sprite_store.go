// SpriteStore bundles a texture atlas  with the  cpu ang gpu buffers needed to render
// individual sprites contained in that atlas. The coordinates of each sprite in the
// atlas comes from the AtlasMap struct. All buffers in the store  are preallocated
// and it shoudld be fast to clear and repopulate them constantly (e.g. every frame).
//
// A SpriteStore uses a single OpenGL program that consists of a vertex/fragment
// shader pair (called Shader). If you need sprites rendered with different shaders put
// them in  separate SpriteStores. SpriteStore requires a Shader that has at least
// a vertex and a texture attribute. By convention these have to named "vp" and "vt"
// respectively.
package sprite

import (
	"errors"
	"fmt"
	"image"
	"math/rand"

	"bitbucket.org/n-gl/ngl/shaders"
	"bitbucket.org/tentontrain/math"
	"github.com/go-gl/gl/v4.6-core/gl"
)

// SpriteStore holds data for sprites coming from the same altas
type SpriteStore struct {
	ID       SpriteStoreID
	AtlasMap AtlasMap // named access to sprite locations on atlas

	atlas      uint32 // OpenGL texture containing all sprites
	numSprites int    // Sprites currently stored
	maxSprites int    // Max number of sprites that can be stored

	window math.Vector2[int] // dimesions of render window

	Shader shaders.Shader // shader used for this store (can only have one)

	// CPU side buffers
	cpuBuffers map[string][]float32

	// GPU side buffers
	gpuBuffers map[string]uint32

	vao uint32 // bundles all gpuBuffers into one reference
}

// NewSpriteStoreFromFiles initialises a sprite store given an atlas image and v/f shaders
// It depends on opengl being initialized via Renderer.Init(). The max number of sprites that can
// be stored is fixed to maxSprites and all buffers are preallocated.
func NewSpriteStore(atlasImage *image.RGBA, atlasInfo AtlasMap, shader shaders.Shader, maxSprites int,
	window math.Vector2[int]) (*SpriteStore, error) {

	st := SpriteStore{
		ID:         SpriteStoreID(rand.Int()),
		AtlasMap:   atlasInfo,
		Shader:     shader,
		cpuBuffers: make(map[string][]float32),
		gpuBuffers: make(map[string]uint32),
		maxSprites: maxSprites,
		window:     window,
	}

	var err error
	if st.atlas, err = textureFromRGBA(atlasImage); err != nil {
		return nil, err
	}

	numVertices := 6 // 2 triangles per sprite

	// intialize empty cpu buffers of fixed size
	for name, attr := range shader.Attributes {
		st.cpuBuffers[name] = make([]float32, maxSprites*numVertices*int(attr.Type.Size))
	}

	// initialize empty GPU buffers and bind to vao
	vbos, sizes, locations := []uint32{}, []int32{}, []uint32{}
	for name, attr := range shader.Attributes {
		st.gpuBuffers[name] = newGLBuffer(maxSprites * numVertices * int(attr.Type.Size))
		vbos = append(vbos, st.gpuBuffers[name])
		sizes = append(sizes, attr.Type.Size)
		locations = append(locations, attr.Location)
	}
	st.vao, err = newVAO(vbos, sizes, locations)
	if err != nil {
		return nil, err
	}

	return &st, nil
}

// Initialize a sprite store from disk
func NewSpriteStoreFromFiles(atlasfile, infofile string, shader shaders.Shader, maxSprites int,
	window math.Vector2[int]) (*SpriteStore, error) {

	atlasInfo := AtlasMap{}
	if err := atlasInfo.FromFile(infofile); err != nil {
		return nil, err
	}

	atlasImage, err := rgbaFromFile(atlasfile)
	if err != nil {
		return nil, err
	}

	return NewSpriteStore(atlasImage, atlasInfo, shader, maxSprites, window)
}

// Add a sprite to the store.
func (st *SpriteStore) AddSprite(sprite Sprite) error {
	shaderData := sprite.shaderData
	for _, sh := range shaderData {
		err := st.UpdateBuffers(sh.Name, st.numSprites, sh.Data)
		if err != nil {
			return err
		}
	}
	st.numSprites++
	return nil
}

// Update the data of a spritestore buffer. Can be used to add new data as well (since the buffers are preallocated)
// The index corresponds to sprites and is multiplied by 6 (6 vertices per sprite). The provided data should be a multiple of 6
// but this is not enforced.
func (st *SpriteStore) UpdateBuffers(bufferName string, index int, data []float32) error {
	// TODO: this is in the hotpath, benchmark and possibly disable the check
	if _, ok := st.Shader.Attributes[bufferName]; !ok {
		return errors.New(fmt.Sprint("Buffer not found: ", bufferName))
	}
	typeSize := int(st.Shader.Attributes[bufferName].Type.Size)
	startIndex := index * typeSize * 6
	copy(st.cpuBuffers[bufferName][startIndex:startIndex+len(data)], data)
	updateGLBuffer(st.gpuBuffers[bufferName], data, startIndex)
	return nil
}

// UpdateUniform changes the value of a uniform variable in the shader
func (st *SpriteStore) UpdateUniform(name string, value []float32) error {
	if _, ok := st.Shader.Uniforms[name]; !ok {
		return errors.New("Atttribute not found")
	}

	loc := gl.GetUniformLocation(st.Shader.Program, gl.Str(name+"\x00"))

	switch st.Shader.Uniforms[name].Name {
	case "float":
		gl.Uniform1f(loc, value[0])
	case "mat4":
		gl.UniformMatrix4fv(loc, 1, false, &(value[0]))
	default:
		return errors.New("Unknown uniform name")
	}

	return nil
}

// render every sprite
func (st *SpriteStore) render() {
	gl.UseProgram(st.Shader.Program)
	gl.BindVertexArray(st.vao)
	gl.BindTexture(gl.TEXTURE_2D, st.atlas)

	transform := []float32{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	}

	if err := st.UpdateUniform("matrix", transform); err != nil {
		panic(err)
	}

	//2 triangles per sprite * 3 verts per triangle
	gl.DrawArrays(gl.TRIANGLES, 0, int32(st.numSprites*6))
}

// Clear all sprites from buffers
func (st *SpriteStore) Empty() {
	st.numSprites = 0
}
