package sprite

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
)

// Marshal object and then write to file. Pretty indents the file (human readable),
func marshalFile(object interface{}, filename string, pretty bool) error {
	buffer, err := json.Marshal(object)
	if err != nil {
		return err
	}
	buf := bytes.NewBuffer(buffer)
	if pretty {
		buf = &bytes.Buffer{}
		json.Indent(buf, buffer, "", "\t")
	}
	if err := ioutil.WriteFile(filename, buf.Bytes(), 0644); err != nil {
		return err
	}
	return nil
}

// JSON Unmarshal filename into object
func unmarshalFile(object interface{}, filename string) error {
	dat, err := ioutil.ReadFile(filename)
	if err != nil {
		return errors.New("could not read file " + filename)
	}
	if err := json.Unmarshal(dat, object); err != nil {
		return errors.New("could not decode " + filename)
	}
	return nil
}
