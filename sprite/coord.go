package sprite

import "bitbucket.org/tentontrain/math"

// Convert pixel coordinates x, y to opengl coords. winW/H are the current window dimensions
func pixelToGl(x, y, winW, winH int) (glx, gly float32) {
	glx = 2*float32(x)/float32(winW) - 1
	gly = 2*float32(y)/float32(winH) - 1
	return
}

// Return the size in opengl coords so that the sprite appears at its original size
func fixedSize(w, h, winW, winH int) (glw, glh float32) {
	glw = 2 * float32(w) / float32(winW)
	glh = 2 * float32(h) / float32(winH)
	return
}

// Depth converts an int value to an appropriate depth for sprite placement. The passed value is
// clamped to [0,1000]. Lower values are in front of higher values.
func Depth(d int) float32 {
	d = math.Bound(d, 0, MAX_DEPTH)
	return float32(d) / float32(MAX_DEPTH)
}
