package sprite

import (
	"reflect"
	"testing"

	"bitbucket.org/n-gl/ngl/sprite/shaders"
	"bitbucket.org/tentontrain/math"
)

func TestSpriteSetPosition(t *testing.T) {
	shader := DummyShader()
	store := DummySpriteStore(shader)
	sprite, _ := NewSprite(0, &store)

	sprite.SetPosition(math.Point{10, 20})

	x, y := pixelToGl(10, 20, 200, 200)
	expect := []float32{
		x, y, 0,
		x, y, 0,
		x, y, 0,
		x, y, 0,
		x, y, 0,
		x, y, 0,
	}

	if !reflect.DeepEqual(expect, sprite.shaderData["transform"].Data) {
		t.Error(expect, sprite.shaderData["transform"].Data)
	}
}

func TestSpriteSetDepth(t *testing.T) {
	shader := DummyShader()
	store := DummySpriteStore(shader)
	sprite, _ := NewSprite(0, &store)

	sprite.SetDepth(100)

	d := Depth(100)
	expect := []float32{
		0, 0, d, 0, 0, d, 0, 0, d, 0, 0, d, 0, 0, d, 0, 0, d,
	}

	if !reflect.DeepEqual(expect, sprite.shaderData["transform"].Data) {
		t.Error(expect, sprite.shaderData["transform"].Data)
	}
}

func TestSpriteSetScale(t *testing.T) {
	shader := DummyShader()
	store := DummySpriteStore(shader)
	sprite, _ := NewSprite(0, &store)

	sprite.SetScale(math.Point2f{3, 6})

	expect := []float32{
		3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 3, 6,
	}

	if !reflect.DeepEqual(expect, sprite.shaderData["scale"].Data) {
		t.Error(expect, sprite.shaderData["scale"].Data)
	}
}

func TestSpriteUVs(t *testing.T) {
	shader := DummyShader()
	store := DummySpriteStore(shader)
	sprite, _ := NewSprite(0, &store)

	llx, lly := float32(10/100.0), float32(10/200.0) // lower left
	urx, ury := float32(80/100.0), float32(50/200.0) // upper right
	expect := []float32{
		llx, lly,
		urx, lly,
		llx, ury,
		llx, ury,
		urx, lly,
		urx, ury,
	}

	if !reflect.DeepEqual(expect, sprite.shaderData["uvs"].Data) {
		t.Error(expect, sprite.shaderData["uvs"].Data)
	}
}

func TestSpriteSetSize(t *testing.T) {
	shader := DummyShader()
	store := DummySpriteStore(shader)
	sprite, _ := NewSprite(0, &store)
	sprite.SetSize(math.Point{20, 20})

	expect := []float32{0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2}

	if !reflect.DeepEqual(expect, sprite.shaderData["scale"].Data) {
		t.Error(expect, sprite.shaderData["scale"].Data)
	}
}

func DummyShader() shaders.Shader {
	return shaders.Shader{
		Attributes: map[string]shaders.ShaderAttribute{
			"vertices":  {Name: "vertices", Location: 0, Type: shaders.GLSLType{"vec3", 3}},
			"uvs":       {Name: "uvs", Location: 1, Type: shaders.GLSLType{"vec2", 2}},
			"color":     {Name: "color", Location: 2, Type: shaders.GLSLType{"vec4", 4}},
			"transform": {Name: "transform", Location: 3, Type: shaders.GLSLType{"vec3", 3}},
			"scale":     {Name: "scale", Location: 4, Type: shaders.GLSLType{"vec2", 2}},
		},
	}
}

func DummySpriteStore(shader shaders.Shader) SpriteStore {
	return SpriteStore{
		AtlasMap: AtlasMap{
			Width:  100,
			Height: 200,
			Sprites: []math.Box{
				math.Box{math.Point{10, 10}, math.Point{80, 50}},
			},
			Map: map[string]math.Point{
				"sprite": math.Point{0, 0},
			},
		},
		Shader: shader,
		window: math.Point{200, 200},
	}
}
