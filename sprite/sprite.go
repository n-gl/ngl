package sprite

import (
	"bitbucket.org/n-gl/ngl/color"
	"bitbucket.org/n-gl/ngl/shaders"
	"bitbucket.org/tentontrain/math"
)

const NoopSprite = -1 //sprites with this Id dont get drawn

type SpriteID int

// Sprite holds info used to add a sprite to SpriteStore
type Sprite struct {
	SpriteStore *SpriteStore
	Id          SpriteID
	position    math.Vector2[int]
	depth       int
	scale       math.Vector2[float32]
	shaderData  map[string]*shaders.ShaderAttribute
}

// Create a sprite
func NewSprite(spriteId SpriteID, st *SpriteStore) (Sprite, error) {
	s := Sprite{
		SpriteStore: st,
		Id:          spriteId,
		shaderData:  map[string]*shaders.ShaderAttribute{},
	}

	// allocate arrays for each attribute
	for _, attr := range st.Shader.Attributes {
		newAttr := attr
		newAttr.Data = make([]float32, attr.Type.Size*6)
		// copy default values
		if len(attr.Default) > 0 {
			for i := range newAttr.Data {
				newAttr.Data[i] = attr.Default[i%len(attr.Default)]
			}
		}
		s.shaderData[attr.Name] = &newAttr
	}

	uvs := s.generateSpriteUVs(spriteId)
	copy(s.shaderData["vertex"].Data, spriteTemplate[:]) // these never change
	s.shaderData["uv"].Data = uvs
	return s, nil
}

// *** Setters needed because updating sprite parameters must also update shaderdata ***

// Setter for sprite position. Position is in screen space (pixels)
func (s *Sprite) SetPosition(position math.Vector2[int]) {
	s.position = position

	// convert pixel to opengl coords
	glx, gly := pixelToGl(position.X, position.Y, s.SpriteStore.window.X, s.SpriteStore.window.Y)

	for i := 0; i < len(s.shaderData["transform"].Data); i += 3 {
		s.shaderData["transform"].Data[i] = glx
		s.shaderData["transform"].Data[i+1] = gly
	}
}

func (s *Sprite) GetPosition() math.Vector2[int] {
	return s.position
}

// Setter for sprite depth. Depth can be an integer in [0, MAX_DEPTH]. Lower values are in front.
func (s *Sprite) SetDepth(depth int) {
	s.depth = depth
	d := Depth(depth) // map depth to [0,1]
	for i := 0; i < len(s.shaderData["transform"].Data); i += 3 {
		s.shaderData["transform"].Data[i+2] = d
	}
}

func (s *Sprite) GetDepth() int {
	return s.depth
}

// Setter for sprite scale.
func (s *Sprite) SetScale(scale math.Vector2[float32]) {
	s.scale = scale
	for i := 0; i < len(s.shaderData["scale"].Data); i += 2 {
		s.shaderData["scale"].Data[i] = scale.X
		s.shaderData["scale"].Data[i+1] = scale.Y
	}
}

func (s *Sprite) GetScale() math.Vector2[float32] {
	return s.scale
}

// Sets the sprite's size in pixels
func (s *Sprite) SetSize(size math.Vector2[int]) {
	// we double the normalized size because our opengl viewport is |-1,1|=2
	scale := math.Vector2[float32]{
		X: 2 * float32(size.X) / float32(s.SpriteStore.window.X),
		Y: 2 * float32(size.Y) / float32(s.SpriteStore.window.Y),
	}

	s.SetScale(scale)
}

// Sets the sprite size to its original value as it appears on the atlas map
func (s *Sprite) SetOriginalSize() {
	s.SetSize(s.GetOriginalSize())
}

// Get the sprite's original size which is its bounding box on the atlas map
func (s *Sprite) GetOriginalSize() math.Vector2[int] {
	return s.SpriteStore.AtlasMap.Sprites[s.Id].Size()
}

// Set the sprite color. The same color is applied to every vertice.
func (s *Sprite) SetColor(color color.ColorRGBA) {
	ca := color.ToArray()
	for i := range s.shaderData["color"].Data {
		s.shaderData["color"].Data[i] = ca[i%4]
	}
}

func (s *Sprite) GetColor() color.ColorRGBA {
	return color.NewColorRGBAFromSlice(s.shaderData["color"].Data[0:4])
}

// Create vertex and uv buffers for a sprite
func (s *Sprite) generateSpriteUVs(sprite SpriteID) (uvs []float32) {
	// TODO: cache this part as the mapping never changes
	box := s.SpriteStore.AtlasMap.Sprites[sprite]
	uv_min_x := float32(box.P1.X) / float32(s.SpriteStore.AtlasMap.Width)
	uv_min_y := float32(box.P2.Y) / float32(s.SpriteStore.AtlasMap.Height)
	uv_max_x := float32(box.P2.X) / float32(s.SpriteStore.AtlasMap.Width)
	uv_max_y := float32(box.P1.Y) / float32(s.SpriteStore.AtlasMap.Height)

	uvs = make([]float32, 12)
	uvs[0], uvs[1] = uv_min_x, uv_min_y
	uvs[2], uvs[3] = uv_max_x, uv_min_y
	uvs[4], uvs[5] = uv_min_x, uv_max_y
	uvs[6], uvs[7] = uv_min_x, uv_max_y
	uvs[8], uvs[9] = uv_max_x, uv_min_y
	uvs[10], uvs[11] = uv_max_x, uv_max_y

	return
}
